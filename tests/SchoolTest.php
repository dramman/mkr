<?php

include 'D:\mkr\School.php'; //sorry

use \PHPUnit\Framework\TestCase;

class SchoolTest extends TestCase
{
    private $school;

    protected function setUp(): void
    {
        $this->school = new School('307');
    }

    public function testGetNumberOfStudentsFromGrade(){
        $students = $this->school->getNumberOfStudentsFromGrade('5th');
        $this->assertEquals(2, $students);
    }

    /**
     * @dataProvider addDataProvider
     * @param $expected
     */

    public function testGetBestStudentFromGrade($grade, $expected)
    {
        $result = $this->school->getBestStudentFromGrade($grade);
        $this->assertEquals($expected, $result);
    }

    public function addDataProvider() {
        return array(
            ['6th', 'Stas'],
            ['5th', 'Oleg'],
            ['6th', 'Oleg'],
        );
    }

    protected function tearDown(): void
    {
        unset($this->school);
    }
}